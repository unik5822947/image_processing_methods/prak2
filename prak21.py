import cv2
import argparse
import imutils
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image, ImageFilter

path = 'E:\obrabotka_img\prak2\prak2.1images'
path1 = 'E:\obrabotka_img\prak2\prak2.13x3images'

images = []
def start(images):
  for i in range(1, 5):
    image = cv2.imread(path + f'\image{i}.jpg')
    images.append(image)

  fig, axs = plt.subplots(nrows= 2 , ncols= 2 , figsize=(15, 15))

  fig.suptitle('Исходные изображения')

  axs[0, 0].imshow(cv2.cvtColor(images[0],cv2.COLOR_BGR2RGB))
  axs[0, 1].imshow(cv2.cvtColor(images[1],cv2.COLOR_BGR2RGB))
  axs[1, 0].imshow(cv2.cvtColor(images[2],cv2.COLOR_BGR2RGB))
  axs[1, 1].imshow(cv2.cvtColor(images[3],cv2.COLOR_BGR2RGB))

def median(images):
  #применяем медианный фильтр 3 × 3 к изображениям
  images_rst = []
  for i in range(len(images)):
    images_rst.append(cv2.medianBlur(images[i], 3))

  # выводим изображения
  fig, axs = plt.subplots(nrows= 2 , ncols= 2 , figsize=(15, 15))

  fig.suptitle('Медианный фильтр')

  axs[0, 0].imshow(cv2.cvtColor(images_rst[0],cv2.COLOR_BGR2RGB))
  axs[0, 1].imshow(cv2.cvtColor(images_rst[1],cv2.COLOR_BGR2RGB))
  axs[1, 0].imshow(cv2.cvtColor(images_rst[2],cv2.COLOR_BGR2RGB))
  axs[1, 1].imshow(cv2.cvtColor(images_rst[3],cv2.COLOR_BGR2RGB))

  # Сохраняем изображение в папку с проектом
  cv2.imwrite('prak2.13x3images/image1_1.png', images_rst[0])
  cv2.imwrite('prak2.13x3images/image2_1.png', images_rst[1])
  cv2.imwrite('prak2.13x3images/image3_1.png', images_rst[2])
  cv2.imwrite('prak2.13x3images/image4_1.png', images_rst[3])
  #plt.show()

def laplas(images):
  #применяю фильтр лапласа к изображениям
  for i in range(len(images)):
    new_image = cv2.Laplacian(images[i],cv2.CV_64F)
    plt.figure(figsize=(11,6))
    plt.subplot(121), plt.imshow(images[i]),plt.title('Оригинал')
    plt.xticks([]), plt.yticks([])
    plt.subplot(122), plt.imshow(new_image, cmap='gray'),plt.title('Лаплас')
    plt.xticks([]), plt.yticks([])
    plt.show()

def srCh(images):
  for i in range(len(images)):
    image = cv2.cvtColor(images[i], cv2.COLOR_BGR2HSV) # convert to HSV
    figure_size = 9 # the dimension of the x and y axis of the kernal.
    new_image = cv2.blur(image,(figure_size, figure_size))
    plt.figure(figsize=(11,6))
    plt.subplot(121), plt.imshow(cv2.cvtColor(image, cv2.COLOR_HSV2RGB)),plt.title('Оригинал')
    plt.xticks([]), plt.yticks([])
    plt.subplot(122), plt.imshow(cv2.cvtColor(new_image, cv2.COLOR_HSV2RGB)),plt.title('Фильтр средних частот')
    plt.xticks([]), plt.yticks([])
    plt.show()

start(images)
laplas(images)