import cv2
import argparse
import numpy as np
import imutils

def task():
    path = 'E:/obrabotka_img/prak2/prak23/'

    def set_image(image):
        return cv2.imread(path + image)
    
    def fragment_search():
        image = set_image('image.jpg')
        fragment = set_image('fragment.jpg')
        (fragmentHeight, fragmentWidth) = fragment.shape[:2]
        # Осуществляем поиск фрагмента на изображении
        result = cv2.matchTemplate(image, fragment, cv2.TM_CCOEFF)
        (_, _, minLoc, maxLoc) = cv2.minMaxLoc(result)
        # Выделяем искомый фрагмент и отделяем его от изображения
        topLeft = maxLoc
        botRight = (topLeft[0] + fragmentWidth, topLeft[1] + fragmentHeight)
        roi = image[topLeft[1]:botRight[1], topLeft[0]:botRight[0]]
        # Создаем затемненный прозрачный слой, для затемнения всей части изображения
        mask = np.zeros(image.shape, dtype="uint8")
        image = cv2.addWeighted(image, 0.20, mask, 0.75, 0)
        # Возвращаем искомый фрагмент в изображение
        image[topLeft[1]:botRight[1], topLeft[0]:botRight[0]] = roi
        # Вывод фрагмента и изображения в отдельные окна (закрываются при нажатии любой клавиши на клавиатуре)
        cv2.imshow("Image", imutils.resize(image, height=900))
        cv2.imshow("Et", fragment)
        cv2.waitKey(0)

        cv2.imwrite(f"E:/obrabotka_img/prak2/result23/fragment_search.jpg", image)
            
    
    fragment_search()

task()